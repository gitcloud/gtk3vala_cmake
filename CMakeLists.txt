###
# Copyright (c) 2009, Paul Gideon Dann
#
# Permission to use, copy, modify, and/or distribute this software for any
# purpose with or without fee is hereby granted, provided that the above
# copyright notice and this permission notice appear in all copies.
#
# THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
# WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
# MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
# ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
###

cmake_minimum_required(VERSION 3.3.0)

project(gtktest)

list(APPEND CMAKE_MODULE_PATH ${CMAKE_SOURCE_DIR}/cmake/vala)

# Default configuration values. These must be before the project command or
# they won't work in Windows.
# If no build type is specified, default to "Release"
if (NOT CMAKE_BUILD_TYPE)
  set(CMAKE_BUILD_TYPE Release CACHE STRING
      "None Debug Release RelWithDebInfo MinSizeRel"
      FORCE)
endif()

# Global CMake options
set(CMAKE_INCLUDE_CURRENT_DIR ON)

if (NOT MSVC)
  # Enable the C++11 standard
  set(CMAKE_CXX_FLAGS ${CMAKE_CXX_FLAGS} -std=c++11)
endif()

# Configure Vala
find_package(Vala "0.30" REQUIRED)
include(${VALA_USE_FILE})

# Configure GTK3
find_package(PkgConfig)
pkg_check_modules(GTK3 gtk+-3.0)

vala_precompile(VALA_C
  SOURCES
    gtktest.vala
  PACKAGES
    gtk+-3.0
#  DIRECTORY
#    gen
#     OPTIONS
#       --thread
#     CUSTOM_VAPIS
#       some_vapi.vapi
#     GENERATE_VAPI
#       myvapi
#     GENERATE_HEADER
#       myheader
)

add_executable(gtktest ${VALA_C})
target_compile_options(gtktest PRIVATE ${GTK3_CFLAGS} ${GTK3_CFLAGS_OTHER})
target_link_libraries(gtktest ${GTK3_LIBRARIES})
